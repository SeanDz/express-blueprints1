var express = require('express')

var app = express() //app is then an invocation of require('express')

app.get('/', function (request, response, next) {
    response.send('Hello, World!')
})

app.listen(3000)
console.log('Express started on port 3000')

app.set('view engine', 'jade')
app.set('views', __dirname + '/views')

app.get('/', function(request, response, next) {
    res.render('index')
})

app.engine('jade', require('jade').__express)
app.engine('html', require('ejs').__express)
app.get('/html', function(request, response, next) {
    response.render('index.html')
})

app.get('/jade', function(request, response, next) {
    response.render('index.jade')
    )}